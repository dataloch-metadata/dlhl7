import hl7
from hl7.mllp import start_hl7_server
import asyncio
import aiorun
import configparser
import datetime
import re
import pathlib
import datetime
import os

config = configparser.ConfigParser()
config.read('config.ini')
    
HL7PORT = config.get('Connections', 'HL7PORT')
base_directory = config.get('Path', 'path')

def create_folder(path):
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)  

def save_file(message, port):
     
    if str(message.segment('MSH')[3][0][0]) == 'MINDRAY_EGATEWAY':
            message_second = message.segment('OBR')[7]
            message_date   = str(message_second)[0:8]
            message_ward   = message.segment('PV1')[3][0][0]
            message_bed    = message.segment('PV1')[3][0][9] 
            filename = f'{message_ward}_{message_bed}_{message_second}.hl7'
            base_path = f'{base_directory}/{message_date}/{message_ward}/{message_bed}'
    else:
        current_time = datetime.datetime.now()
        date = current_time.strftime("%Y%m%d")
        filename = f'{current_time}_from_{port}.hl7'
        base_path = f'{base_directory}/{date}'

    create_folder(base_path)
    filepath = os.path.join(base_path, filename)
    with open(filepath, 'w+') as message_file:
        print(f"{message}", file=message_file)

async def handle_hl7_connection(hl7_reader, hl7_writer):
    """This will be called every time a socket connects
    with us.
    """
    peername = hl7_writer.get_extra_info("peername")
    port = re.search(r' \d+', str(peername)).group().replace(" ", "")
    print(f"\nConnection established {peername}")
    try:
        # We're going to keep listening until the writer
        # is closed. Only writers have closed status.
        while not hl7_writer.is_closing():
            hl7_message = await hl7_reader.readmessage()
            print(f'\nMessage received from {port}, the file is {len(str(hl7_message))} characters\' long.')
            save_file(hl7_message, port)
            # Now let's send the ACK and wait for the
            # writer to drain
            hl7_writer.writemessage(hl7_message.create_ack())
            await hl7_writer.drain()
    except asyncio.IncompleteReadError:
        # Oops, something went wrong, if the writer is not
        # closed or closing, close it.
        if not hl7_writer.is_closing():
            hl7_writer.close()
            await hl7_writer.wait_closed()
    print(f"\nConnection closed {peername}")

async def hl7_server():
    try:
        # Start the server in a with clause to make sure we
        # close it
        async with await start_hl7_server(
            handle_hl7_connection, port=HL7PORT
        ) as hl7_server:
            await hl7_server.serve_forever()
    except asyncio.CancelledError:
        # Cancelled errors are expected
        pass
    except Exception as ex:
        print("Error occurred in hl7 server: ")
        print(ex)


aiorun.run(hl7_server(),stop_on_unhandled_errors=True)
