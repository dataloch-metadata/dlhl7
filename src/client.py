import hl7
from hl7.client import MLLPClient
from hl7.mllp import open_hl7_connection
import asyncio
import aiorun
import argparse
import configparser


config = configparser.ConfigParser()
config.read('config.ini')
    
HL7PORT = config.getint('Connections', 'HL7PORT')
LOCALHOST = config.get('Connections', 'LOCALHOST')


# Message obtained from https://confluence.hl7.org/download/attachments/49644116/ADT_A01%20-%201.txt?api=v2
SAMPLE_HL7_MESSAGE = """MSH|^~\&|ADT1|GOOD HEALTH HOSPITAL|GHH LAB, INC.|GOOD HEALTH HOSPITAL|198808181126|SECURITY|ADT^A01^ADT_A01|MSG00001|P|2.8||
EVN|A01|200708181123||
PID|1||PATID1234^5^M11^ADT1^MR^GOOD HEALTH HOSPITAL~123456789^^^USSSA^SS||EVERYMAN^ADAM^A^III||19610615|M||C|2222 HOME STREET^^GREENSBORO^NC^27401-1020|GL|(555) 555-2004|(555)555-2004||S||PATID12345001^2^M10^ADT1^AN^A|444333333|987654^NC|
NK1|1|NUCLEAR^NELDA^W|SPO^SPOUSE||||NK^NEXT OF KIN
PV1|1|I|2000^2012^01||||004777^ATTEND^AARON^A|||SUR||||ADM|A0|
""".replace('\n','\r')

# Test message set to have date from 2022
SAMPLE_HL7_MESSAGE = """MSH|^~\&|MINDRAY_EGATEWAY^00A0370027A71E80^EUI-64|MINDRAY|||20220820202831.0000+0100||ORU^R01^ORU_R01|9497756|P|2.6|||AL|NE||UNICODE UTF-8|||IHE_PCD_001^IHE PCD^1.3.6.1.4.1.19376.1.6.1.1.1^ISO
PID|||^^^Hospital^PI||^^^^^^L
PV1||I|RIE118^^RIE118_B2^^^^^^00-0F-14-04-58-C5^BED2
OBR|1||9497756^MINDRAY_EGATEWAY^00A0370027A71E80^EUI-64|100001^CONTINUOUS WAVEFORM^99MNDRY|||20220814202829.0000+0100|20220814202830.0000+0100
""".replace('\n','\r')


def hl7_client():
    # Create client: encoding is utf-8 by default.
    print(LOCALHOST,HL7PORT)
    with MLLPClient(LOCALHOST,HL7PORT) as client:
        client.send_message(SAMPLE_HL7_MESSAGE)

async def hl7_async_client():
    hl7_reader, hl7_writer = await asyncio.wait_for(
                open_hl7_connection(LOCALHOST,HL7PORT),
                timeout = 10
            )
    hl7_message = hl7.parse(SAMPLE_HL7_MESSAGE)
    hl7_writer.writemessage(hl7_message)
    await hl7_writer.drain()
    print(f'Sent message async\n {hl7_message}'.replace('\r', '\n'))
    hl7_ack = await asyncio.wait_for(
            hl7_reader.readmessage(),
            timeout=10
        )
    print(f"Received ACK\n {hl7_ack}".replace('\r','\n'))


argparser = argparse.ArgumentParser(description='HL7 test')
action = argparser.add_mutually_exclusive_group(required=True)
action.add_argument('-c','--client', action='store_true', help='Send a HL7 message')
action.add_argument('-a','--asyncclient', action='store_true', help='Send a HL7 message async')
args = argparser.parse_args()

if args.client:
    hl7_client()
elif args.asyncclient:
    aiorun.run(hl7_async_client(),stop_on_unhandled_errors=True)
else:
    print("No option to run.")
