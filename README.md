Two test scripts: for the test's sake I've written a client-side script, which sends a message that the server side can receive. There are two options for the client side: asyncronous (to be called with -a comm. line argument) and synchronous (-c arg). The synchronous version closes the connection automatically, the async one doesn't.

On the server side: I am saving the file that is sent in a folder - one folder is made per day, and the file name is dat-time + port that is came from.

The scripts use python-hl7 API (https://python-hl7.readthedocs.io/en/latest/api.html), which can process HL7 version 2, but not version 3. If that turns out to be an issue later, we need to look for a different library to use, but it did not seem trivial when I was checking.